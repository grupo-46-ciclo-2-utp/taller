import java.util.ArrayList;
import java.util.List;

public class Recepcionista extends Empleado {
    /**************
     * Atributos
     *********/
    private List<Cliente> clientes;
    
    /******
     * Constructor
     *******/
    public Recepcionista(String nombre, String apellido, String cedula){
        super(nombre, apellido, cedula);
        this.clientes = new ArrayList<Cliente>();
    }
    /********
     * metodos
     * (acciones)
     *********/
    public void registrar_clientes(String nombre, String apellido, String cedula){
        //aqui construir un objeto cliente
        Cliente objCliente = new Cliente(nombre, apellido, cedula);
        //almacenar el objeto arraylist
        this.clientes.add(objCliente);
    }

    public void registrar_vehiculos(){
        //aqui construir un objeto de tipo vehiculo
    }
}
