/********
 * Autor: Yonatan Nicolas Llanos Ruiz
 * Fecha: 06/Agosto/2021
 * Empresa: TIC UTP
 * Ciudad: Manizales-Caldas
 * Descripcion Proyecto: Base de datos taller de Automoviles
 */
public class Empleado {
    /********
     * Atributos
     ********/
    private String nombre;
    private String apellido;
    private String cedula;
    private double sueldo;

    /**********
     * Constructor
     *******/
    public Empleado(String nombre, String apellido, String cedula){
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
    }

    /**********
     * Consultores
     * getters
     **********/
    public String getNombre(){
        return this.nombre;
    }

    public String getApellido(){
        return this.apellido;
    }
}
